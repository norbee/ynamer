FROM python:3.7

ENV PYTHONUNBUFFERED=1

WORKDIR /code

RUN apt-get update && apt-get install -y netcat

COPY deploy/env/requirements.txt /code/
RUN pip install -r requirements.txt
RUN pip install gunicorn

COPY . /code/

ENTRYPOINT [ "/code/deploy/docker/entrypoint.sh" ]
