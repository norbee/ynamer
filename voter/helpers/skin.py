class SkinHelper():
    DEFAULT_SKIN = 'default'
    AVAILABLE_SKINS = [
        'default',
        'cerulean',
        'cosmo',
        'cyborg',
        'darkly',
        'flatly',
        'journal',
        'lumen',
        'paper',
        'readable',
        'sandstone',
        'simplex',
        'slate',
        'solar',
        'spacelab',
        'superhero',
        'united',
        'yeti'
    ]
