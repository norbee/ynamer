from voter.models import Vote, User, Child


class VoteHelper():
    def get_vote_counts(user: User, child: Child, all_name_count: int = None):
        no_way_count = Vote.objects.filter(
            user_id=user.id,
            score=Vote.SCORE_NO_WAY,
            child_id=child.id
        ).count()
        why_not_count = Vote.objects.filter(
            user_id=user.id,
            score=Vote.SCORE_WHY_NOT,
            child_id=child.id
        ).count()
        ranked_count = Vote.objects.filter(
            user_id=user.id,
            score__gt=Vote.SCORE_WHY_NOT,
            child_id=child.id
        ).count()

        if all_name_count:
            to_vote_count = all_name_count - no_way_count - ranked_count
        else:
            to_vote_count = None

        return no_way_count, why_not_count, ranked_count, to_vote_count
