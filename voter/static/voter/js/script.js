/**
 * Main APP functions.
 */
var app = {
    init: function() {
        $('#result-container').find('.nav-tabs').each(function() {
            var $firstTab = $(this).find('a:first');

            app.loadTabContent($firstTab);
            $firstTab.tab('show');
        });
    },
    disableVoteButtons: function() {
        $('.loader').delay(500).fadeIn();
        $('button.vote-btn').addClass('disabled');
        $('a.vote-btn').addClass('disabled');
        $('body').addClass('busy');
    },
    getAsyncData: function(url, $target) {
        var $loader = $target.siblings('div.async-loader');

        $.get(url)
            .done(function(data) {
                $target.html(data);
                $loader.fadeOut(200);
            })
            .fail(function(e) {
                $loader.removeClass('alert-info').addClass('alert-danger');
            });
    },
    loadTabContent: function($tab) {
        var url = $tab.data('async');
        if (url === undefined) {
            return;
        }

        var targetId = $tab.attr('href').substring(1),
            $target = $('#' + targetId).find('div.' + 'async-content');

        app.getAsyncData(url, $target);
    },
    changeLanguage: function (lang) {
        var $form = $('#language-selector-form'),
            $input = $form.find('input[name="language"]');

        $input.val(lang);
        $form.submit();
    }
};

/**
 * Function bindings.
 */
$(document).ready(function() {
    // App init
    app.init();

    // Child selector
    $('#child-selector').change(function() {
       $(this).parents('form').submit();
    });

    // Vote button disabling after click
    $('a.vote-btn').click(function () {
        app.disableVoteButtons();
    });
    $('#vote-form').submit(function() {
        app.disableVoteButtons();
    });

    // Tab selector async data load
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        app.loadTabContent($(e.currentTarget));
    });

    // Language selector form submit
    $('a.language-selector-link').click(function(e) {
        e.preventDefault();
        app.changeLanguage($(this).data('lang'));
    });
});