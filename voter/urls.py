from django.conf.urls import url, include

from voter.views import main, user, vote, asyncx

app_name = 'voter'

async_patterns = [
    url(r'^all-my-votes$', asyncx.get_all_my_votes, name='async-all-my-votes'),
    url(r'^all-names$', asyncx.get_all_names, name='async-all-names'),
    url(r'^my-best-names$', asyncx.get_my_best_names, name='async-my-best-names'),
    url(r'^common-best-names$', asyncx.get_common_best_names, name='async-common-best-names'),
    url(r'^common-total-names$', asyncx.get_common_total_names, name='async-common-total-names')
]

urlpatterns = [
    url(r'^$', main.index, name='index'),
    url(r'^login$', user.user_login, name='login'),
    url(r'^logout$', user.user_logout, name='logout'),
    url(r'^signup$', user.user_signup, name='signup'),
    url(r'^skin/change$', user.skin_change, name='skin-change'),
    url(r'^child/change$', user.child_change, name='child-change'),
    url(r'^child/save$', user.child_save, name='child-save'),
    url(r'^vote/(?P<type>first-round|second-round|random|change)$', vote.index, name='vote'),
    url(r'^vote/save$', vote.save, name='vote-save'),
    url(r'^async/', include(async_patterns)),
    url(r'^i18n/', include('django.conf.urls.i18n'))
]
