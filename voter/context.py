from django.conf import settings
from voter.helpers.cookie import CookieHelper
from voter.helpers.skin import SkinHelper


def add_context_variables(request):
    try:
        build = open('version.info').read()
    except IOError:
        build = 'local'

    try:
        skin = request.COOKIES[CookieHelper.KEY_SELECTED_SKIN]
        if skin not in SkinHelper.AVAILABLE_SKINS:
            skin = SkinHelper.DEFAULT_SKIN
    except KeyError:
        skin = SkinHelper.DEFAULT_SKIN

    return {
        'BRAND': 'yNameR',
        'BUILD': build,
        'SKIN': skin,
        'YNAMER_ENV': settings.YNAMER_ENV
    }
