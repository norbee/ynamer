import csv

from urllib.request import urlopen, Request
from django.core.management.base import BaseCommand
from django.utils import timezone
from voter.models import ChildName, Language


class Command(BaseCommand):
    help = 'Imports the child names from the specified locations'

    url_list = {
        'HU_BOY': {
            'url': 'http://www.nytud.mta.hu/oszt/nyelvmuvelo/utonevek/osszesffi.txt',
            'language': 'HU',
            'sex': ChildName.BOY,
            'encoding': 'iso8859_2',
            'header': True
        },
        'HU_GIRL': {
            'url': 'http://www.nytud.mta.hu/oszt/nyelvmuvelo/utonevek/osszesnoi.txt',
            'language': 'HU',
            'sex': ChildName.GIRL,
            'encoding': 'iso8859_2',
            'header': True
        },
    }

    def handle(self, *args, **options):
        for title, data in self.url_list.items():
            self.stdout.write(self.style.SUCCESS(title))

            language = Language.objects.get(code=data['language'])
            child_names = ChildName.objects.filter(language=language, sex=data['sex'])
            current_names = []
            for child_name in child_names:
                current_names.append(child_name.name)

            request = Request(data['url'])
            content = urlopen(request)

            cr = csv.reader(content.read().decode(data['encoding']).splitlines())
            if data['header']:
                next(cr)
            count = 0
            for row in cr:
                name = row[0]
                if name not in current_names:
                    child_name = ChildName(language=language, sex=data['sex'], name=name, created_at=timezone.now())
                    child_name.save()
                    self.stdout.write('.', ending='')
                    self.stdout.flush()
                    count += 1
                # TODO implement name disabling

            self.stdout.write('\n' + str(count) + ' imported.')

        self.stdout.write(self.style.SUCCESS('Done.'))