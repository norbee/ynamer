from django import forms
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from voter.models import Child, Family


class ChildForm(forms.Form):
    family_id = forms.CharField(
        widget=forms.HiddenInput()
    )
    title = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Code name / title'),
                'aria-describedby': 'child-title-icon'
            }
        )
    )
    sex = forms.ChoiceField(
        choices=Child.SEX_TYPES,
        widget=forms.RadioSelect()
    )

    def save(self):
        child = Child(
            title=self.cleaned_data['title'],
            sex=self.cleaned_data['sex'],
            family=Family.objects.get(id=self.cleaned_data['family_id']),
            created_at=timezone.now()
        )
        child.save()
