from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate
from django import forms


class LoginForm(forms.Form):
    user = None
    username = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Username'),
                'aria-describedby': 'login-username-icon'
            }
        )
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Password'),
                'aria-describedby': 'login-password-icon'
            }
        )
    )

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        user = authenticate(
            username=cleaned_data['username'],
            password=cleaned_data['password']
        )
        if user is None:
            raise forms.ValidationError(_('User not found. Try again, or sign up!'))
        else:
            self.user = user
