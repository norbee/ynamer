from django import forms
from django.db import transaction
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.utils.translation import ugettext_lazy as _
from voter.models import Language, Child, Family


class SignupForm(forms.Form):
    mom_email = forms.EmailField(
        max_length=50,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Email'),
                'aria-describedby': 'mom-email-icon'
            }
        )
    )
    mom_username = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Username'),
                'aria-describedby': 'mom-username-icon'
            }
        )
    )
    mom_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Password'),
                'aria-describedby': 'mom-password-icon'
            }
        )
    )
    dad_email = forms.EmailField(
        max_length=50,
        widget=forms.EmailInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Email'),
                'aria-describedby': 'dad-email-icon'
            }
        )
    )
    dad_username = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Username'),
                'aria-describedby': 'dad-username-icon'
            }
        )
    )
    dad_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Password'),
                'aria-describedby': 'dad-password-icon'
            }
        )
    )
    family_name = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Family name'),
                'aria-describedby': 'family-name-icon'
            }
        )
    )
    language = forms.ChoiceField(
        choices=[('', '')],
        widget=forms.Select(
            attrs={
                'class': 'form-control',
                'aria-describedby': 'language-icon'
            }
        )
    )
    child_title = forms.CharField(
        max_length=50,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Code name / title'),
                'aria-describedby': 'child-title-icon'
            }
        )
    )
    sex = forms.ChoiceField(
        choices=Child.SEX_TYPES,
        widget=forms.RadioSelect()
    )

    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__(*args, **kwargs)

        # Load choices here so db calls are not made during migrations.
        self.fields['language'].choices = [('', _('Language'))] + [(lang.code, _(lang.title)) for lang in Language.objects.all()]

    def clean(self):
        cleaned_data = super(SignupForm, self).clean()
        valid_languages = [] + [lang.code for lang in Language.objects.only('code').all()]

        if cleaned_data['language'] not in valid_languages:
            raise forms.ValidationError(_('The given language is not valid, please select from the list!'))

        user = User.objects.filter(email=cleaned_data['mom_email']).first()
        if user is not None:
            raise forms.ValidationError(_("Mommy's email address already exists, please try another!"))

        user = User.objects.filter(username=cleaned_data['mom_username']).first()
        if user is not None:
            raise forms.ValidationError(_("Mommy's username already exists, please try another!"))

        user = User.objects.filter(email=cleaned_data['dad_email']).first()
        if user is not None:
            raise forms.ValidationError(_("Daddy's email address already exists, please try another!"))

        user = User.objects.filter(username=cleaned_data['dad_username']).first()
        if user is not None:
            raise forms.ValidationError(_("Daddy's username already exists, please try another!"))

    @transaction.atomic
    def signup(self):
        mommy = User(
            email=self.cleaned_data['mom_email'],
            username=self.cleaned_data['mom_username'],
            password=make_password(self.cleaned_data['mom_password']),
            date_joined=timezone.now()
        )
        mommy.save()

        daddy = User(
            email=self.cleaned_data['dad_email'],
            username=self.cleaned_data['dad_username'],
            password=make_password(self.cleaned_data['dad_password']),
            date_joined=timezone.now()
        )
        daddy.save()

        family = Family(
            last_name=self.cleaned_data['family_name'],
            language=Language.objects.get(code=self.cleaned_data['language']),
            husband=daddy,
            wife=mommy,
            created_at=timezone.now()
        )
        family.save()

        child = Child(
            title=self.cleaned_data['child_title'],
            sex=self.cleaned_data['sex'],
            family=family,
            created_at=timezone.now()
        )
        child.save()
