from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth import login, logout
from voter.models import Child, Family
from voter.forms.login import LoginForm
from voter.forms.signup import SignupForm
from voter.forms.child import ChildForm
from voter.helpers.session import SessionHelper
from voter.helpers.skin import SkinHelper
from voter.helpers.cookie import CookieHelper


def user_login(request):
    if request.method == 'POST':
        login_form = LoginForm(request.POST)

        if login_form.is_valid():
            login(request, login_form.user)
            return redirect(reverse('voter:index'))
    else:
        login_form = LoginForm()

    params = {
        'login_form': login_form
    }

    return render(request, 'voter/login.html', params)


def user_logout(request):
    logout(request)
    return redirect(reverse('voter:login'))


def user_signup(request):
    if request.method == 'POST':
        signup_form = SignupForm(request.POST)

        if signup_form.is_valid():
            signup_form.signup()
            # TODO implement success message
            return redirect(reverse('voter:login'))
    else:
        signup_form = SignupForm()

    params = {
        'signup_form': signup_form
    }

    return render(request, 'voter/signup.html', params)


def child_change(request):
    if request.method != 'POST' or not request.user.is_authenticated:
        return redirect(reverse('voter:index'))

    selected_child_id = request.POST.get('selected_child_id', None)
    family = Family.get_by_user(request.user)

    try:
        Child.objects.get(id=selected_child_id, family=family)
        request.session.update({
            SessionHelper.KEY_ACTIVE_CHILD_ID: int(selected_child_id)
        })
    except Child.DoesNotExist:
        # TODO error handling
        pass

    return redirect(reverse('voter:index'))


def child_save(request):
    if request.method != 'POST' or not request.user.is_authenticated:
        return redirect(reverse('voter:index'))

    child_form = ChildForm(request.POST)
    if child_form.is_valid():
        child_form.save()

    return redirect(reverse('voter:index'))


def skin_change(request):
    skin = request.GET.get('s', None)

    response = redirect(reverse('voter:index'))

    if skin in SkinHelper.AVAILABLE_SKINS:
        response.set_cookie(CookieHelper.KEY_SELECTED_SKIN, skin, CookieHelper.TTL_SELECTED_SKIN)

    return response
