from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from voter.models import ChildName, Vote, Child, Family
from voter.forms.child import ChildForm
from voter.helpers.vote import VoteHelper
from voter.helpers.session import SessionHelper


def index(request):
    params = {}

    # TODO handle default language (if no family?!)
    if request.user.is_authenticated:
        family = Family.get_by_user(request.user)
        children = Child.objects.filter(family_id=family.id)
        spouse = family.get_other_user(user=request.user)

        session = request.session
        child_id = session.get(SessionHelper.KEY_ACTIVE_CHILD_ID, None)
        language_code = session.get(SessionHelper.KEY_FAMILY_LANGUAGE_CODE, None)

        if language_code is None:
            language_code = family.language.code
            session.update({
                SessionHelper.KEY_FAMILY_LANGUAGE_CODE: language_code
            })

        params = {
            'family': family,
            'children': children,
            'spouse': spouse
        }

        if child_id is None:
            if children.count() == 1:
                child_id = children[0].id
                request.session['active_child_id'] = child_id
                # TODO this is the problem

        # TODO optimise to not send so many queries
        if child_id is not None:
            child = Child.objects.get(id=child_id)

            all_name_count = ChildName.objects.filter(language=language_code, sex=child.sex).count()

            my_no_way_count, my_why_not_count, my_ranked_count, my_to_vote_count = \
                VoteHelper.get_vote_counts(request.user, child, all_name_count)

            sps_to_vote_count = VoteHelper.get_vote_counts(spouse, child, all_name_count)[3]

            child_form = ChildForm(initial={'family_id': family.id})

            params.update({
                'active_child': child,
                'child_form': child_form,
                'all_name_count': all_name_count,
                'my_progress': 100 - round((my_to_vote_count / all_name_count) * 100) if all_name_count > 0 else 0,
                'my_no_way_count': my_no_way_count,
                'my_why_not_count': my_why_not_count,
                'my_ranked_count': my_ranked_count,
                'my_to_vote_count': my_to_vote_count,
                'sps_to_vote_count': sps_to_vote_count,
                'sps_progress': 100 - round((sps_to_vote_count / all_name_count) * 100) if all_name_count > 0 else 0,
                'common_total_active': my_to_vote_count <= 0 and sps_to_vote_count <= 0
            })
    else:
        return redirect(reverse('voter:login'))

    return render(request, 'voter/index.html', params)
