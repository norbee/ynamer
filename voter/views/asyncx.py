from django.http import HttpResponse
from django.shortcuts import render_to_response
from voter.models import Vote, VoteLog, ChildName, Child, Family
from voter.helpers.session import SessionHelper


def __init(request):
    response = None

    if not request.user.is_authenticated:
        response = HttpResponse('Unauthorized', status=401)

    active_child_id = request.session.get(SessionHelper.KEY_ACTIVE_CHILD_ID, None)
    if active_child_id is None:
        response = HttpResponse('Forbidden', status=403)

    return response is None, active_child_id, response


def get_my_best_names(request):
    is_valid, active_child_id, response = __init(request)
    if not is_valid:
        return response

    my_best_names = Vote.objects.filter(
        user_id=request.user.id,
        score__gt=Vote.SCORE_WHY_NOT,
        child_id=active_child_id
    ).order_by('-score')

    params = {
        'my_best_names': my_best_names
    }

    return render_to_response('voter/async/my-best-names.html', params)


def get_all_my_votes(request):
    is_valid, active_child_id, response = __init(request)
    if not is_valid:
        return response

    my_votes = VoteLog.objects.filter(user_id=request.user.id, child_id=active_child_id).order_by('-created_at')

    params = {
        'my_votes': my_votes
    }

    return render_to_response('voter/async/all-my-votes.html', params)


def get_all_names(request):
    is_valid, active_child_id, response = __init(request)
    if not is_valid:
        return response

    active_child = Child.objects.get(id=active_child_id)
    all_names_with_score = ChildName.get_all_with_vote(user=request.user, child=active_child)

    params = {
        'names': all_names_with_score
    }

    return render_to_response('voter/async/all-names-with-score.html', params)


def get_common_best_names(request):
    is_valid, active_child_id, response = __init(request)
    if not is_valid:
        return response

    active_child = Child.objects.get(id=active_child_id)
    family = Family.get_by_user(request.user)
    spouse = family.get_other_user(user=request.user)

    common_best_names = ChildName.get_common_best_list(user=request.user, spouse=spouse, child=active_child)

    params = {
        'common_best_names': common_best_names
    }

    return render_to_response('voter/async/common-best-names.html', params)


def get_common_total_names(request):
    is_valid, active_child_id, response = __init(request)
    if not is_valid:
        return response

    active_child = Child.objects.get(id=active_child_id)
    family = Family.get_by_user(request.user)
    spouse = family.get_other_user(user=request.user)

    common_total_names = ChildName.get_common_total_list(user=request.user, spouse=spouse, child=active_child)

    params = {
        'user': request.user,
        'spouse': spouse,
        'common_total_names': common_total_names
    }

    return render_to_response('voter/async/common-total-names.html', params)
