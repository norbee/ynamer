from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect, reverse
from django.utils import timezone
from django.middleware import csrf
from random import randint
from voter.models import ChildName, Vote, Family, Child, VoteLog
from voter.helpers.vote import VoteHelper
from voter.helpers.session import SessionHelper


@csrf_exempt
def index(request, type=None):
    index_url = reverse('voter:index')

    active_child_id = request.session.get(SessionHelper.KEY_ACTIVE_CHILD_ID, None)
    language_code = request.session.get(SessionHelper.KEY_FAMILY_LANGUAGE_CODE, None)
    if not request.user.is_authenticated or active_child_id is None or language_code is None:
        return redirect(index_url)

    active_child = Child.objects.get(id=active_child_id)
    change_child_name_id = request.POST.get('child_name_id', None)
    is_step_back = bool(request.POST.get('step_back', False))
    previous_type = request.POST.get('previous_type', None)
    previous_child_name_id = request.session.get(SessionHelper.KEY_LAST_VOTE_CHILD_NAME_ID, None)

    if previous_child_name_id:
        del request.session[SessionHelper.KEY_LAST_VOTE_CHILD_NAME_ID]

    to_vote_count = None
    first_round_count = None
    second_round_count = None

    last_vote = None
    is_change_mode = False

    if type == Vote.TYPE_CHANGE:
        if change_child_name_id:
            try:
                child_name = ChildName.objects.get(id=change_child_name_id)
                last_score = None
                try:
                    last_vote = Vote.objects.get(child_name=child_name, child=active_child, user=request.user)
                    last_score = last_vote.score
                except Vote.DoesNotExist:
                    pass

                child_name.last_score = last_score
                is_change_mode = True
            except ChildName.DoesNotExist:
                return redirect(index_url)
        else:
            return redirect(index_url)
    else:
        all_names_count = ChildName.objects.filter(language=language_code, sex=active_child.sex).count()
        no_way_count, why_not_count, ranked_count, to_vote_count = \
            VoteHelper.get_vote_counts(request.user, active_child, all_names_count)

        if to_vote_count == 0:
            return redirect(index_url)

        first_round_count = to_vote_count - why_not_count
        second_round_count = why_not_count

        if (type == Vote.TYPE_FIRST_ROUND and first_round_count == 0) \
                or (type == Vote.TYPE_SECOND_ROUND and second_round_count == 0):
            return redirect(reverse('voter:vote', kwargs={'type': Vote.TYPE_RANDOM}))

        not_voted_names = ChildName.get_not_voted_list(user=request.user, child=active_child, vote_type=type)
        max_index = len(list(not_voted_names)) - 1
        child_name = not_voted_names[randint(0, max_index)]

    back_url = reverse('voter:vote', kwargs={'type': previous_type}) if is_step_back else index_url

    params = {
        'family': Family.get_by_user(request.user),
        'child_name': child_name,
        'total_count': to_vote_count,
        'first_round_count': first_round_count,
        'second_round_count': second_round_count,
        'active_child': active_child,
        'change_mode': is_change_mode,
        'last_vote': last_vote,
        'type': type,
        'previous_child_name_id': previous_child_name_id,
        'previous_type': previous_type,
        'back_url': back_url
    }

    return render(request, 'voter/vote.html', params)


def save(request):
    active_child_id = request.session.get(SessionHelper.KEY_ACTIVE_CHILD_ID, None)
    if not request.user.is_authenticated or request.method != 'POST' or active_child_id is None:
        return redirect(reverse('voter:index'))

    # to avoid double votes with re-post
    csrf.rotate_token(request)

    error = False

    score = request.POST.get('score', None)
    child_name_id = request.POST.get('child_name_id', None)
    vote_type = request.POST.get('vote_type', 'random')
    redirect_url = request.POST.get('redirect', reverse('voter:vote', kwargs={'type': vote_type}))

    if child_name_id is None or score is None or int(score) < -1 or int(score) > 6:
        error = True

    try:
        ChildName.objects.get(pk=child_name_id)
    except ChildName.DoesNotExist:
        # TODO error handling (display)
        error = True

    if not error:
        try:
            vote = Vote.objects.get(
                user_id=request.user.id,
                child_name_id=child_name_id,
                child_id=active_child_id,
            )
            vote.score = score
            vote.updated_at = timezone.now()
        except Vote.DoesNotExist:
            vote = Vote(
                score=score,
                user_id=request.user.id,
                child_name_id=child_name_id,
                child_id=active_child_id,
                created_at=timezone.now()
            )
        vote.save()

        VoteLog(
            score=score,
            user_id=request.user.id,
            child_name_id=child_name_id,
            child_id=active_child_id,
            created_at=timezone.now()
        ).save()

        if vote_type != Vote.TYPE_CHANGE:
            request.session[SessionHelper.KEY_LAST_VOTE_CHILD_NAME_ID] = child_name_id

    return redirect(redirect_url)
