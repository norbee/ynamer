from django.contrib import admin
from .models import Language, Family, ChildName, Vote, Child, VoteLog

admin.site.register(Language)
admin.site.register(Family)
admin.site.register(ChildName)
admin.site.register(Vote)
admin.site.register(Child)
admin.site.register(VoteLog)
