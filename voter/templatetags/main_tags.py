from django import template
from voter.helpers.skin import SkinHelper

register = template.Library()


@register.inclusion_tag('voter/tags/skin-selector.html')
def skin_selector(selected_skin):
    return {
        'skins': SkinHelper.AVAILABLE_SKINS,
        'selected_skin': selected_skin
    }


@register.inclusion_tag('voter/tags/language-selector.html')
def language_selector():
    return {}


@register.inclusion_tag('voter/tags/skin-css.html')
def skin_css(skin):
    css_path = None
    if skin and skin in SkinHelper.AVAILABLE_SKINS:
        css_path = '/voter/css/skin/' + skin + '.css'

    return {
        'is_default': skin == SkinHelper.DEFAULT_SKIN,
        'css_path': css_path
    }
