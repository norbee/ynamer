from django import template

register = template.Library()


@register.inclusion_tag('voter/tags/starbutton.html')
def star_button(value=1, score=None):
    return {
        'value': value,
        'range': range(value),
        'disabled': True if value == score else False
    }


@register.inclusion_tag('voter/tags/stars.html')
def stars(value=1, type='normal'):
    is_int = True
    try:
        value = int(value)
    except ValueError:
        is_int = False

    params = {
        'is_int': is_int,
        'class_fix': '-empty' if type == 'empty' else ''
    }

    if is_int:
        params['range'] = range(value if value > 0 else 0)

    return params
