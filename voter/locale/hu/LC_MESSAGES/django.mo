��    E      D  a   l      �      �  !        4     Q  	   ^     h     t     x          �     �     �     �     �     �     �  9   �  4        M     R     X     _     n     z     �     �     �  
   �     �     �  
   �  	   �     �     �     �  9   �  4   7     l  
   z     �  	   �     �     �     �     �     �     �     �     �     �     	     	     	     	     %	     *	     /	     6	  !   =	  =   _	  7   �	     �	     �	  &   �	     
     
     
     
  g  &
  )   �  *   �  %   �     	          )     5     =     D     Q     h     m     �     �     �  
   �  S   �  Z        s  
   y     �     �     �     �  
   �     �     �     �     �                    *     9     H  T   T  Z   �  
             #     (     4     L     b     k     s     �     �  	   �  
   �     �     �     �     �     �     �     �     �     �  J     M   X     �     �  C   �     �  	   	  
     
        @          6                       3   4   ;      	           (   &                         !      =   7   C   #   "   %   >   A       2   )   ?          E      5   *      ,               .                            0                    B   8      9      '   $   D                1   -                      +   <   
   :                /                   %(my_to_vote_count)s names to go %(sps_to_vote_count)s names to go %(total_count)s names to go! All My Votes All Names Baby's Data Boy Cancel Change Change score Child Code name / title Common Best Common Score Common Total Daddy's Data Daddy's email address already exists, please try another! Daddy's username already exists, please try another! Done Email Family Family Results Family name Family's Data First Round Girl Language Last Score Last name, congratulations! Left Loading... Logged in Login Logout Mommy's Data Mommy's email address already exists, please try another! Mommy's username already exists, please try another! My Best Names My Results Name New child No names found yet. No votes found yet. No way! Password Please select the child! Previous Progress Random Ranked Save Score Second Round Signup Skin Skip Spouse Submit The best tool to choose the name! The given language is not valid, please select from the list! This list is only available when both parents are done! Time Total User not found. Try again, or sign up! Username Vote Welcome Why not? Project-Id-Version: ynamer
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-07-31 21:37+0200
PO-Revision-Date: 2017-08-02 21:28+0200
Last-Translator: Norbert Lehotzky
Language-Team: myself
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.1
 %(my_to_vote_count)s név van még hátra %(sps_to_vote_count)s név van még hátra %(total_count)s név van még hátra! Összes szavazatom Összes név Baba adatai Kisfiú Mégse Módosítás Szavazat módosítása Baba Fedőnév / megnevezés Közös legjobbak Közös eredmény Közös összes Apa adatai Apa email címe már létezik az adatbázisban, kérlek próbálj másikat megadni! Apa felhasználó neve már létezik az adatbázisban, kérlek próbálj másikat megadni! Elég Email cím Család Családi eredmények Családnév Család adatai Első kör Kislány Nyelv Legutóbbi szavazat Utolsó név, gratulálok! Maradék Betöltés... Bejelentkezve Bejelentkezés Kijelentkezés Anya adatai Anya email címe már létezik az adatbázisban, kérlek próbálj másikat megadni! Anya felhasználóneve már létezik az adatbázisban, kérlek próbálj másikat megadni! Kedvenceim Saját eredményeim Név Új gyermek Nem találhatók nevek. Nincsenek szavazatok. Kizárt! Jelszó Válassz gyermeket! Előző Folyamat Véletlen Rangsorolt Mentés Szavazat Második kör Regisztráció Stílus Újat Társ Hajrá A legjobb választás! A megadott nyelv nem érvényes, kérlek válassz a lehetőségek közül! Ez a lista csak akkor elérhető ha mindkét szülő végzett a szavazással! Idő Összes A felhasználó nem található, próbáld újra vagy regisztrálj! Felhasználónév Szavazás Üdvözlet Miért ne? 