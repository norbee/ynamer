��    E      D  a   l      �      �  !        4     Q  	   ^     h     t     x          �     �     �     �     �     �     �  9   �  4        M     R     X     _     n     z     �     �     �  
   �     �     �  
   �  	   �     �     �     �  9   �  4   7     l  
   z     �  	   �     �     �     �     �     �     �     �     �     �     	     	     	     	     %	     *	     /	     6	  !   =	  =   _	  7   �	     �	     �	  &   �	     
     
     
     
  g  &
  '   �  (   �  #   �       
        !     -  	   1     ;     C     V  	   [     e     v     �     �  E   �  C   �     0     6     =     E     Y     f     x     �     �     �     �     �     �  
   �  	   �  	   �     �  E   �  C   @     �     �     �  
   �     �     �     �     �                 	   "     ,  	   5     ?     H     U     ^  	   c     m     u     ~  I   �  >   �       	     O   &     v  
   �  
   �     �     @          6                       3   4   ;      	           (   &                         !      =   7   C   #   "   %   >   A       2   )   ?          E      5   *      ,               .                            0                    B   8      9      '   $   D                1   -                      +   <   
   :                /                   %(my_to_vote_count)s names to go %(sps_to_vote_count)s names to go %(total_count)s names to go! All My Votes All Names Baby's Data Boy Cancel Change Change score Child Code name / title Common Best Common Score Common Total Daddy's Data Daddy's email address already exists, please try another! Daddy's username already exists, please try another! Done Email Family Family Results Family name Family's Data First Round Girl Language Last Score Last name, congratulations! Left Loading... Logged in Login Logout Mommy's Data Mommy's email address already exists, please try another! Mommy's username already exists, please try another! My Best Names My Results Name New child No names found yet. No votes found yet. No way! Password Please select the child! Previous Progress Random Ranked Save Score Second Round Signup Skin Skip Spouse Submit The best tool to choose the name! The given language is not valid, please select from the list! This list is only available when both parents are done! Time Total User not found. Try again, or sign up! Username Vote Welcome Why not? Project-Id-Version: ynamer
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-07-31 21:37+0200
PO-Revision-Date: 2017-09-13 21:08+0200
Last-Translator: Norbert Lehotzky
Language-Team: myself
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.1
 %(my_to_vote_count)s Namen bis zum Ende %(sps_to_vote_count)s Namen bis zum Ende %(total_count)s Namen bis zum Ende! Alle meine stimmen Alle Namen Babys Daten Bub Abbrechen Ändern Abstimmung ändern Kind Werktitel Beste gemeinsame Gemeinsame Ergebnis Alle gemeinsame Papas Daten Papas E-Mail-Adresse ist bereits vorhanden, bitte gib eine andere an! Papas Benutzername ist bereits vorhanden, bitte gib eine andere an! Genug E-Mail Familie Familien Ergebnisse Familienname Daten der Familie Erste Runde Mädchen Sprache Letzte Abstimmung Letzer Name, gratulation! Rest Laden... Eingeloggt Einloggen Ausloggen Mamas Daten Mamas E-Mail-Adresse ist bereits vorhanden, bitte gib eine andere an! Mamas Benutzername ist bereits vorhanden, bitte gib eine andere an! Meine Lieblingsnamen Meine Ergebnisse Name Neues Kind Keine Name gefunden. Keine abstimmungen gefunden. Zicher nicht! Passwort Bitte wählen! Voriger Prozess Zufällig Rangiert Speichern Ergebnis Zweite Runde Anmelden Stil Auslassen Partner Schicken Die beste Wahl! Die angegebene Sprache ist nicht gültig, bitte aus der Liste auswählen! Diese Liste ist nur verfügbar, wenn beide Eltern fertig sind! Zeit Insgesamt Benutzer nicht gefunden. Versuchen Sie es noch einmal, oder melden Sie sich an! Benutzername Abstimmung Willkommen Warum nicht? 