from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class Language(models.Model):
    code = models.CharField(max_length=2, unique=True)
    title = models.CharField(max_length=25)

    def __str__(self):
        return self.title


class Family(models.Model):
    language = models.ForeignKey(Language, to_field='code', db_column='language')
    last_name = models.CharField(max_length=100)
    husband = models.ForeignKey(User, related_name='husband')
    wife = models.ForeignKey(User, related_name='wife')
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.last_name + ' (' + self.husband.username + ' & ' + self.wife.username + ')'

    @staticmethod
    def get_by_user(user: User):
        return Family.objects.get(models.Q(husband_id=user.id) | models.Q(wife_id=user.id))

    def get_other_user(self, user: User):
        return self.husband if user == self.wife else self.wife


class Child(models.Model):
    BOY = 'B'
    GIRL = 'G'
    SEX_TYPES = (
        (BOY, _('Boy')),
        (GIRL, _('Girl'))
    )

    family = models.ForeignKey(Family)
    title = models.CharField(max_length=25)
    sex = models.CharField(max_length=1, choices=SEX_TYPES, default=BOY)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.family.last_name + ' ' + self.title


class ChildName(models.Model):
    BOY = 'B'
    GIRL = 'G'
    SEX_TYPES = (
        (BOY, _('Boy')),
        (GIRL, _('Girl'))
    )

    language = models.ForeignKey(Language, to_field='code', db_column='language')
    name = models.CharField(max_length=100)
    sex = models.CharField(max_length=1, choices=SEX_TYPES, default=BOY)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name + ' (' + self.get_sex_display() + ')'

    @staticmethod
    def get_not_voted_list(user: User, child: Child, vote_type=None):
        if vote_type == Vote.TYPE_FIRST_ROUND:
            where = 'V.score IS NULL'
        elif vote_type == Vote.TYPE_SECOND_ROUND:
            where = 'V.score = 0'
        else:
            where = 'V.score IS NULL OR V.score = 0'

        return ChildName.objects.raw("""
            SELECT
              CN.*,
              V.score AS last_score
            FROM
              voter_childname CN
              LEFT JOIN voter_vote V
                ON V.child_name_id = CN.id
                AND V.user_id = %s
                AND V.child_id = %s
            WHERE
              CN.language = %s
              AND CN.sex = %s
              AND ({where})
            ORDER BY
              name;
            """.format(where=where), [user.id, child.id, child.family.language.code, child.sex]
        )

    @staticmethod
    def get_common_best_list(user: User, spouse: User, child: Child):
        return ChildName.objects.raw("""
            SELECT
              CN.*,
              VM.score AS my_score,
              VS.score AS sps_score,
              VM.score + VS.score AS common_score
            FROM
              voter_childname CN
              JOIN voter_vote VM
                ON VM.child_name_id = CN.id
                AND VM.user_id = %s
                AND VM.child_id = %s
                AND VM.score > 0
              JOIN voter_vote VS
                ON VS.child_name_id = CN.id
                AND VS.user_id = %s
                AND VS.child_id = %s
                AND VS.score > 0
            WHERE
              CN.language = %s
              AND CN.sex = %s
            ORDER BY
              common_score DESC;
            """, [user.id, child.id, spouse.id, child.id, child.family.language.code, child.sex]
        )

    @staticmethod
    def get_common_total_list(user: User, spouse: User, child: Child):
        return ChildName.objects.raw("""
            SELECT
              CN.*,
              IFNULL(VM.score, 0) AS my_score,
              IFNULL(VS.score, 0) AS sps_score,
              (SELECT CASE WHEN IFNULL(VM.score, 0) > IFNULL(VS.score, 0) THEN VM.score ELSE VS.score END) AS max_score,
              IFNULL(VM.score, 0) + IFNULL(VS.score, 0) as sum_score
            FROM
              voter_childname CN
              LEFT JOIN voter_vote VM
                ON VM.child_name_id = CN.id
                AND VM.user_id = %s
                AND VM.child_id = %s
                AND VM.score > 0
              LEFT JOIN voter_vote VS
                ON VS.child_name_id = CN.id
                AND VS.user_id = %s
                AND VS.child_id = %s
                AND VS.score > 0
            WHERE
              CN.language = %s
              AND CN.sex = %s
              AND (
                VM.score > 0 OR VS.score > 0
              )
            ORDER BY
              max_score DESC, sum_score DESC, my_score DESC, sps_score DESC;
            """, [user.id, child.id, spouse.id, child.id, child.family.language.code, child.sex]
        )

    @staticmethod
    def get_all_with_vote(user: User, child: Child):
        return ChildName.objects.raw("""
            SELECT
              CN.*,
              V.score AS my_score
            FROM
              voter_childname CN
              LEFT JOIN voter_vote V
                ON V.child_name_id = CN.id
                AND V.user_id = %s
                AND V.child_id = %s
            WHERE
              CN.language = %s
              AND CN.sex = %s
            ORDER BY
              name;
        """, [user.id, child.id, child.family.language.code, child.sex]
        )

    # TODO FIX this my_score issue
    def get_my_score_name(self):
        if self.my_score == Vote.SCORE_NO_WAY:
            return _('No way!')
        elif self.my_score == Vote.SCORE_WHY_NOT:
            return _('Why not?')
        else:
            return self.my_score if self.my_score is not None else ''


class Vote(models.Model):
    TYPE_FIRST_ROUND = 'first-round'
    TYPE_SECOND_ROUND = 'second-round'
    TYPE_RANDOM = 'random'
    TYPE_CHANGE = 'change'

    SCORE_NO_WAY = -1
    SCORE_WHY_NOT = 0

    user = models.ForeignKey(User)
    child = models.ForeignKey(Child)
    child_name = models.ForeignKey(ChildName)
    score = models.SmallIntegerField(null=True)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(null=True, default=None)

    class Meta:
        unique_together = ('user', 'child', 'child_name')

    def __str__(self):
        return self.user.username + ' - ' + self.child_name.name + ' => ' + str(self.score)


class VoteLog(models.Model):
    SCORE_NO_WAY = -1
    SCORE_WHY_NOT = 0

    user = models.ForeignKey(User)
    child = models.ForeignKey(Child)
    child_name = models.ForeignKey(ChildName)
    score = models.SmallIntegerField(null=True)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.user.username + ' - ' + self.child_name.name + ' => ' + str(self.score)

    def get_score_name(self):
        if self.score == self.SCORE_NO_WAY:
            return _('No way!')
        elif self.score == self.SCORE_WHY_NOT:
            return _('Why not?')
        else:
            return self.score
