#!/usr/bin/env bash

rm -rf .hg
rm .hgignore
rm .hgflow
rm README.md
rm bitbucket-pipelines.yml

mkdir -p ~/.ssh
cat deploy/bitbucket/ssh-key/id_rsa >> ~/.ssh/id_rsa
cat deploy/bitbucket/ssh-key/known_hosts >> ~/.ssh/known_hosts
chmod 600 ~/.ssh/id_rsa
chmod 600 ~/.ssh/known_hosts

tar -zcvf /tmp/$BITBUCKET_REPO_SLUG-$BITBUCKET_COMMIT.tar.gz .
scp -rp /tmp/$BITBUCKET_REPO_SLUG-$BITBUCKET_COMMIT.tar.gz codeship@178.62.202.119:/home/codeship/deployment/

# webhook
# wget --post-data "valami=macska&kutya=kalap" http://webhook.lehotzky.com/ynamer.php >> /dev/null
