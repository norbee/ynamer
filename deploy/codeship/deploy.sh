#!/usr/bin/env bash

rm -rf .hg
rm .hgignore
rm .hgflow
rm README.md
tar -zcvf /tmp/ynamer-$CI_BRANCH-$CI_BUILD_NUMBER.tar.gz .
scp -rp /tmp/ynamer-$CI_BRANCH-$CI_BUILD_NUMBER.tar.gz codeship@178.62.202.119:/home/codeship/deployment/