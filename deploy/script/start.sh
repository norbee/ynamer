#!/usr/bin/env bash

# must be run from the root directory
# ./deploy/script/start.sh

virtualenv -p python3 env
source env/bin/activate
pip install -r deploy/env/requirements.txt

python manage.py migrate
python manage.py collectstatic --noinput

uwsgi --virtualenv env --socket ynamer.sock --module ynamer.wsgi --chmod-socket=666 &>/dev/null &