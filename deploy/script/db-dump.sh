#!/usr/bin/env bash

python manage.py dumpdata --indent 2 --exclude auth.permission --exclude contenttypes > db_dump.json
