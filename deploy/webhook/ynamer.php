<?php

$data = file_get_contents('php://input');
$data = json_decode($data, true);

if (!isset($data['build'])) {
    echo 'error';
    die();
}

$project = 'ynamer';
$branch  = $data['build']['branch'];
$build   = $data['build']['build_id'];
$status  = $data['build']['status'];

if ($branch != 'default' || $status != 'success') {
    echo 'error';
    die();
}

$basePath   = '/home/codeship/deployment/';
$deployPath = $basePath . $project . '-' . $branch . '-' . $build;
$wwwPath    = '/www/' . $project;

$startFile   = $deployPath . '/deploy/script/start.sh';
$stopFile    = $deployPath . '/deploy/script/stop.sh';
$versionFile = $deployPath . '/version.info';

try {
    $phar = new PharData($deployPath . '.tar.gz');
    $phar->extractTo($deployPath, null, true);

    chdir($deployPath);
    $hgTags = file_get_contents($deployPath . '/.hgtags');
    $version = end(explode(' ', $hgTags));
    file_put_contents($versionFile, $version);

    if (is_dir($wwwPath)) {
        exec($stopFile);
        unlink($wwwPath);
    }

    symlink($deployPath, $wwwPath);
    exec($startFile);

    echo 'success';
} catch (Exception $e) {
	echo 'error';
}
