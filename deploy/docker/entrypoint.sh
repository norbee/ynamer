#!/usr/bin/env bash

if [ "$DB_TYPE" = "mariadb" ]; then
    echo "Waiting for the database..."

    while ! nc -z $DB_HOST $DB_PORT; do
      sleep 0.1
    done

    echo "Database started!"
fi

cd /code
python manage.py makemigrations --noinput
python manage.py migrate --noinput
python manage.py importer
python manage.py collectstatic --noinput

exec "$@"
